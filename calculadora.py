def adicao(a, b):
    return a + b

def subtracao(a, b):
    return a - b

def multiplicacao(a, b):
    return a * b

def divisao(a, b):
    if b != 0:
        return a / b
    else:
        return "Erro: Divisão por zero!"

def calculadora(escolha, num1, num2):
    if escolha == '1':
        return adicao(num1, num2)
    elif escolha == '2':
        return subtracao(num1, num2)
    elif escolha == '3':
        return multiplicacao(num1, num2)
    elif escolha == '4':
        return divisao(num1, num2)
    else:
        return "Escolha inválida!"

escolha = "1"
num1 =  45
num2 = 23

if escolha in {'1', '2', '3', '4'}:
    resultado = calculadora(escolha, num1, num2)
    print("Resultado:", resultado)
else:
    print("Escolha inválida!")
